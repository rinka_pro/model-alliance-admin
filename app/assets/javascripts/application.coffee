#= require jquery/dist/jquery.min
#= require jquery_ujs

#= require sugar/release/sugar-full.min

#= require moment/min/moment-with-locales.min

#= require angular/angular.min
#= require angular-i18n/angular-locale_ru-ru
#= require angular-route/angular-route.min
#= require angular-resource/angular-resource.min
#= require angularjs/rails/resource
#= require angular-bootstrap/ui-bootstrap.min
#= require angular-bootstrap/ui-bootstrap-tpls.min

#= require angular-simple-logger/dist/angular-simple-logger.min
#= require lodash/lodash.min
#= require angular-google-maps/dist/angular-google-maps.min

#= require main
#= require_tree ./angular/factories
#= require_tree ./angular/controllers
#= require_tree .
