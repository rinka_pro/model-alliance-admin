angular.module( 'AdminApp' ).factory 'Profession', (Base) ->
	class Profession extends Base
		@configure
			url: '/angular/users/professions'
			name: 'profession'
