angular.module( 'AdminApp' ).factory 'City', (Base) ->
	class City extends Base
		@configure
			url: '/angular/cities'
			name: 'city'
