angular.module( 'AdminApp' ).factory 'News', (Base) ->
	class News extends Base
		@configure
			url: '/angular/news/{{id}}'
			name: 'news'
