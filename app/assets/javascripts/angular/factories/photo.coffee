angular.module( 'AdminApp' ).factory 'Photo', (Base) ->
	class Photo extends Base
		@configure
			url: '/angular/photos/{{id}}'
			name: 'photo'

