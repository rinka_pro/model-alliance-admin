angular.module( 'AdminApp' ).factory 'Base', (RailsResource) ->
	class Base extends RailsResource
		do @configure
		@intercept 'response', (result, resourceConstructor, context) ->
			if result.data?
				if result.originalData.meta? and result.originalData.meta instanceof Object
					Object.each result.originalData.meta, (key, value) ->
						result.data["$#{key}"] = value
			result
