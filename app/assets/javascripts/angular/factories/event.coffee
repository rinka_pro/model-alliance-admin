angular.module( 'AdminApp' ).factory 'Event', (Base) ->
	class Event extends Base
		@configure
			url: '/angular/events/{{id}}'
			name: 'event'
