angular.module( 'AdminApp' ).factory 'User', (Base) ->
	class User extends Base
		@configure
			url: '/angular/users/{{id}}'
			name: 'user'
			fullResponse: true
			serializer: 'UserSerializer'
