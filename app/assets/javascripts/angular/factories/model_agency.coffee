angular.module( 'AdminApp' ).factory 'ModelAgency', (Base) ->
	class ModelAgency extends Base
		@configure
			url: '/angular/model_agencies'
			name: 'modelAgency'
