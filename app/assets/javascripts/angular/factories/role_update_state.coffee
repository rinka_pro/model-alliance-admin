angular.module( 'AdminApp' ).factory 'RoleUpdateState', (Base) ->
	class RoleUpdateState extends Base
		@configure
			url: '/angular/users/role_updates/states'
			name: 'roleUpdateState'
			serializer: 'RoleUpdateStateSerializer'
