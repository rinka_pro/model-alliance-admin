angular.module( 'AdminApp' ).factory 'RoleUpdateRequest', (Base) ->
	class RoleUpdateRequest extends Base
		@configure
			url: '/angular/users/role_updates/requests/{{id}}'
			name: 'roleUpdateRequest'
			serializer: 'RoleUpdateRequestSerializer'
