angular.module( 'AdminApp' ).factory 'Comment', ($resource) ->
	$resource '/angular/comments/:target_type/:target_id'
