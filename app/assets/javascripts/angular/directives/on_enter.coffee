angular.module( 'AdminApp' ).directive 'onEnter', ->
	restrict: 'A'
	link: (scope, element, attrs) ->
		element.bind 'keydown keypress', (event) ->
			if event.which == 13
				do event.preventDefault
				scope.$apply ->
					scope.$eval attrs.onEnter
