angular.module( 'AdminApp' ).controller 'NewsController', ($scope, $location, News) ->
	$scope.itemsPerPage = 20
	$scope.news = []
	$scope.pagesCount = 0

	$scope.init = ->
		News.query().then (result) ->
			$scope.news = result
			$scope.pagesCount = $scope.news.length / $scope.itemsPerPage
			$scope.newsPerPage = $scope.news.first $scope.itemsPerPage

	$scope.show = (id) ->
		$location.url '/news/' + id

	$scope.create = ->
		$location.url '/news/create'

	$scope.changePage = ->
		start = ($scope.currentPage - 1) * $scope.itemsPerPage
		finish = $scope.currentPage * $scope.itemsPerPage - 1
		$scope.newsPerPage = $scope.news[start .. finish]
