angular.module( 'AdminApp' ).controller 'NewsShowController', ($scope, $location, News, City) ->
	City.query().then (result) -> $scope.cities = result.cities

	$scope.init = (id) ->
		if $scope.isExists = id?
			News.get(id).then (result) -> $scope.record = result
		else
			$scope.record = new News
			$scope.record.cities = []


	$scope.cancel =  ->
		$location.url '/news'

	$scope.add =  ->
		$scope.record.cities.push $scope.city

	$scope.del = (city) ->
		$scope.record.cities.remove city
