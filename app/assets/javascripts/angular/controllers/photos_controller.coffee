angular.module( 'AdminApp' ).controller 'PhotosController', ($scope, $location, Photo, Comment) ->
	$scope.itemsPerPage = 20
	$scope.photos = []
	$scope.pagesCount = 0

	$scope.init = ->
		Photo.query().then (result) ->
			$scope.photos = result
			$scope.pagesCount = $scope.photos.length / $scope.itemsPerPage
			$scope.photosPerPage = $scope.photos.first $scope.itemsPerPage

	$scope.edit = (id) ->
		console.log 'Editing: ' + id
		$scope.editedId = id

	$scope.isEdited = (id) ->
		$scope.editedId == id

	$scope.comment = (id) ->
		console.log 'commenting: ' + id
		$scope.commentedId = id
		Comment.get {target_type: 'Core::Photo', target_id: id}, (result) ->
			$scope.comments = result.comments
			console.log $scope.comments

	$scope.isCommented = (id) ->
		$scope.commentedId == id

	do $scope.cancel = ->
		$scope.editedId = null
		$scope.commentedId = null

	$scope.changePage = ->
		start = ($scope.currentPage - 1) * $scope.itemsPerPage
		finish = $scope.currentPage * $scope.itemsPerPage - 1
		$scope.photosPerPage = $scope.photos[start .. finish]
