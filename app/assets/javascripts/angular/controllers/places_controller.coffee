angular.module( 'AdminApp' ).controller 'PlacesController', ($scope, $location, Place) ->
	$scope.itemsPerPage = 20
	$scope.places = []
	$scope.pagesCount = 0

	$scope.init = ->
		Place.query().then (result) ->
			$scope.places = result
			$scope.pagesCount = $scope.places.length / $scope.itemsPerPage
			$scope.placesPerPage = $scope.places.first $scope.itemsPerPage

	$scope.show = (id) ->
		$location.url '/places/' + id

	$scope.create = ->
		$location.url '/places/create'

	$scope.changePage = ->
		start = ($scope.currentPage - 1) * $scope.itemsPerPage
		finish = $scope.currentPage * $scope.itemsPerPage - 1
		$scope.placesPerPage = $scope.places[start .. finish]
