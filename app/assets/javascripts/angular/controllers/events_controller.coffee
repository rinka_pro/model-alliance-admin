angular.module( 'AdminApp' ).controller 'EventsController', ($scope, $location, Event) ->
	$scope.itemsPerPage = 20
	$scope.events = []
	$scope.pagesCount = 0

	$scope.init = ->
		Event.query().then (result) ->
			$scope.events = result
			$scope.pagesCount = $scope.events.length / $scope.itemsPerPage
			$scope.eventsPerPage = $scope.events.first $scope.itemsPerPage
			console.log $scope.itemsPerPage

	$scope.show = (id) ->
		$location.url '/events/' + id

	$scope.create = ->
		$location.url '/events/create'

	$scope.changePage = ->
		start = ($scope.currentPage - 1) * $scope.itemsPerPage
		finish = $scope.currentPage * $scope.itemsPerPage - 1
		$scope.eventsPerPage = $scope.events[start .. finish]
