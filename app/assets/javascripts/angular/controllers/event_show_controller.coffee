angular.module( 'AdminApp' ).controller 'EventShowController', ($scope, $location, $element, City, Event, Discount, uiGmapGoogleMapApi) ->
	Event.query().then (result) -> $scope.events = result.events
	City.query().then (result) -> $scope.cities = result.cities

	uiGmapGoogleMapApi.then (maps) ->
		$scope.googleVersion = maps.version
		maps.visualRefresh = true
		$scope.map =
			center:
				latitude: 35
				longitude: 50
			zoom: 4
			options:
				scrollwheel: true

	$scope.cancel =  -> $location.url '/events'
	$scope.currentLanguage = 'ru'
	$scope.categories = Event.categories

	$scope.init = (id) ->
		if $scope.isExists = id?
			Event.get(id).then (result) -> $scope.event = result
		else
			$scope.record = new Event

	['ru', 'en', 'de', 'cn'].each (language) -> $scope[language] = -> $scope.currentLanguage = language
