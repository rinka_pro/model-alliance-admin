angular.module( 'AdminApp' ).controller 'NavigationController', ($scope, $location) ->
	$scope.goTo = (url) -> $location.url url
