angular.module( 'AdminApp' ).controller 'UsersController', ($scope, $location, User) ->
	$scope.itemsPerPage = 20
	$scope.users = []

	$scope.init = ->
		User.query().then (result) ->
			$scope.users = result.data
			$scope.totalUsersCount = result.originalData.count

	$scope.show = (id) -> $location.url '/users/' + id
	$scope.changePage = -> User.query(page: $scope.currentPage).then (result) -> $scope.users = result.data
