angular.module( 'AdminApp' ).controller 'PlaceShowController', ($scope, $location, $element, City, Place, Discount, uiGmapGoogleMapApi) ->
	City.query().then (result) -> $scope.cities = result.cities

	autocomplete = new google.maps.places.Autocomplete $element.find('.address')[0],
		types: ['geocode']
	.addListener 'place_changed', ->
		location = @getPlace().geometry?.location
		if location
			$scope.$apply ->
				$scope.place.location.latitude = location.lat()
				$scope.place.location.longitude = location.lng()
				$scope.map.center =
					latitude: $scope.place.location.latitude
					longitude: $scope.place.location.longitude
		else
			alert 'Location not found for current address!'

	$scope.cancel =  -> $location.url '/places'
	$scope.currentLanguage = 'ru'
	$scope.categories = Place.categories
	$scope.discounts = Discount.variants

	$scope.init = (id) ->
		if $scope.isExists = id?
			Place.get(id).then (result) ->
				$scope.place = result
				$scope.map =
					center:
						latitude: $scope.place.location.latitude
						longitude: $scope.place.location.longitude
					zoom: 15
					mapTypeId: google.maps.MapTypeId.ROADMAP
					options:
						scrollwheel: true
						minZoom: 3
						maxZoom: 17
					marker:
						location:$scope.place.location
						id: $scope.place.location.id
						options:
							draggable: true
						events:
							dragend: (e) ->
								position =
									lat: e.getPosition().lat()
									lng: e.getPosition().lng()

								geocoder = new google.maps.Geocoder
								geocoder.geocode { location: position }, (result, status) ->
									switch status
										when google.maps.GeocoderStatus.OK then $scope.$apply -> $scope.place.location.address = result[0].formatted_address
										when google.maps.GeocoderStatus.ZERO_RESULTS then alert 'No address results found!'
										when google.maps.GeocoderStatus.OVER_QUERY_LIMIT then alert 'Over query limit error!'
										when google.maps.GeocoderStatus.REQUEST_DENIED then alert 'Request denied for geocoding location!'
										when google.maps.GeocoderStatus.INVALID_REQUEST then alert 'Invalid request for geocoding location!'
										else alert 'Unknown error for geocoding location!'
		else
			$scope.record = new Place

	['ru', 'en', 'de', 'cn'].each (language) -> $scope[language] = -> $scope.currentLanguage = language
