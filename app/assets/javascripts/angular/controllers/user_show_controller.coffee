angular.module( 'AdminApp' ).controller 'UserShowController', ($scope, $location, User, ModelAgency, Profession) ->
	ModelAgency.query().then (result) -> $scope.agencies = result.modelAgencies
	Profession.query().then (result) -> $scope.professions = result

	$scope.init   = (id)     -> User.get(id).then (result) -> $scope.user = result.data
	$scope.del    = (photo)  -> $scope.user.photos.remove photo
	$scope.cancel =          -> $location.url '/users'
	$scope.add    =          ->
		agency = $scope.user.modelAgencies.find (object) -> object.id == $scope.agency.id
		$scope.user.modelAgencies.push $scope.agency unless agency

	$scope.modelNames = [
		'guest'
		'talent'
		'fashion_model'
		'celebrity'
	]
