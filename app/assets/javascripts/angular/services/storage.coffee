angular.module( 'AdminApp' ).service 'Storage', ->
	class Storage
		constructor: ->
			@storage = {}

		get: (key) ->
			@storage[key]

		set: (key, value) ->
			@storage[key] = value

		has: (key) ->
			Object.has @storage[key]

		remove: (key) ->
			delete @storage[key]

	new Storage
