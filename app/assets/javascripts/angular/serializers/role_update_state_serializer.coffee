angular.module( 'AdminApp' ).factory 'RoleUpdateStateSerializer', (railsSerializer) ->
	railsSerializer ->
		@resource 'modelAgencies', 'ModelAgency'
		@rename 'modelAgencies', 'agencyTargetsAttributes'
