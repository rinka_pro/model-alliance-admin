angular.module( 'AdminApp' ).factory 'RoleUpdateRequestSerializer', (railsSerializer) ->
	railsSerializer ->
		@resource 'before', 'RoleUpdateState'
		@resource 'after', 'RoleUpdateState'
