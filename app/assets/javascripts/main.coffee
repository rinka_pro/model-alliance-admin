angular.module 'AdminApp', [ 'rails', 'ngResource', 'ngRoute', 'ui.bootstrap', 'ui.bootstrap.tpls', 'uiGmapgoogle-maps' ]

.config ($locationProvider) ->
	$locationProvider.html5Mode
		enabled: true
		requireBase: false

.config ($httpProvider) ->
	token = document.querySelector( 'meta[name=csrf-token]' )?.content
	$httpProvider.defaults.headers.common[ 'x-CSRF-Token' ] = token
	$httpProvider.defaults.headers.common[ 'X-Angular' ] = true

.config ($routeProvider) ->
	$routeProvider.when ':path*',
		templateUrl: (params) ->
			path = params.path
			delete params.path
			path += "?#{ Object.toQueryString params }" if Object.toQueryString(params) != ''
			path
		reloadOnSearch: false
