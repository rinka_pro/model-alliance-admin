class Admin::LoginActionSerializer < ActiveModel::Serializer
	root :user

	attributes :id, :login, :email, :phone, :first_name, :last_name, :avatar
	has_many :devices, serializer: DeviceSerializer

	def avatar
		object.avatar.url
	end
end
