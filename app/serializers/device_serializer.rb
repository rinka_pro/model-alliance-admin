class DeviceSerializer < ActiveModel::Serializer
	root :device
	attributes :id, :device_id, :platform, :app_version
end
