class CommentSerializer < ActiveModel::Serializer
	root :comment

	attributes :id, :body, :avatar

	def avatar
		object.sender.avatar :thumb
	end
end
