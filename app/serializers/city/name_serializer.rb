class City::NameSerializer < ActiveModel::Serializer
	root :city

	attributes :id, :title

	def title
		object.title.ru || object.title.en
	end
end
