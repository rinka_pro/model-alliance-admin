class City::LocalizedSerializer < ActiveModel::Serializer
	root :city
	attributes :id, :title
end
