class Event::DetailedSerializer < ActiveModel::Serializer
	root :event

	attributes :id, :title, :description, :start, :finish, :likes_count, :additional_likes

	has_one :location, serializer: Place::LocationSerializer
	has_many :shared_photos, serializer: Photo::SharedSerializer
end
