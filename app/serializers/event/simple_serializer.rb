class Event::SimpleSerializer < ActiveModel::Serializer
	root :event
	attributes :id, :title, :city_name, :address, :likes_count

	def title
		object.title.ru
	end

	def city_name
		name = object.location.city.title
		name.ru || name.en
	end

	def address
		object.location.address
	end
end
