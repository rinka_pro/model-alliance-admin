class User::RoleUpdate::DetailedSerializer < ActiveModel::Serializer
	root :role_update_request

	has_one :before, serializer: User::RoleUpdate::StateSerializer
	has_one :after, serializer: User::RoleUpdate::StateSerializer

	alias after object

	def before
		object.user
	end
end
