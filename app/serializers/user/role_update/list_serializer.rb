class User::RoleUpdate::ListSerializer < ActiveModel::Serializer
	root :role_update_request

	attributes :id, :login, :selfie, :current_role, :target_role

	def login
		object.user.login
	end

	def current_role
		object.user.role.title
	end

	def target_role
		object.role.title
	end

	def selfie
		object.selfie.url :thumb
	end
end
