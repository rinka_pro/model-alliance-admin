class User::RoleUpdate::StateSerializer < ActiveModel::Serializer
	root :role_update_state

	attributes :id, :first_name, :last_name, :birthday, :selfie
	has_many :model_agencies, serializer: ModelAgencySerializer
	has_many :links, serializer: LinkSerializer

	def selfie
		object.selfie.url :thumb
	end
end
