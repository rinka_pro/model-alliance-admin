class User::PhotoSerializer < ActiveModel::Serializer
	root :photo

	attributes :id, :url, :variant

	def url
		object.source.url :thumb
	end
end
