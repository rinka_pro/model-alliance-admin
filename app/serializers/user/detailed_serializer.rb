class User::DetailedSerializer < ActiveModel::Serializer
	root :user

	attributes :id, :login, :role, :email, :created_at, :phone, :first_name, :last_name, :avatar, :selfie

	has_many :model_agencies, serializer: ModelAgencySerializer
	has_many :links, serializer: LinkSerializer
	has_many :devices, serializer: DeviceSerializer
	has_many :photos, serializer: User::PhotoSerializer

	def avatar
		object.avatar.url
	end

	def selfie
		object.selfie.url
	end

	def role
		object.role.title
	end
end
