class User::ListSerializer < ActiveModel::Serializer
	root :user
	attributes :id, :login, :email, :created_at, :phone, :first_name, :last_name
end
