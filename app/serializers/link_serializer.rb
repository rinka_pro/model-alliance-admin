class LinkSerializer < ActiveModel::Serializer
	root :link
	attributes :id, :variant, :source
end
