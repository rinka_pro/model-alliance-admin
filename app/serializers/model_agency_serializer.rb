class ModelAgencySerializer < ActiveModel::Serializer
	root :model_agency
	attributes :id, :city_name, :title

	def city_name
		object.city.title.en
	end
end
