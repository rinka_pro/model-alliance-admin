class Photo::SharedSerializer < ActiveModel::Serializer
	root :photo

	attributes :id, :url

	def url
		object.source.url :thumb
	end
end
