class Photo::LocationSerializer < ActiveModel::Serializer
	root :location
	attributes :id, :latitude, :longitude, :address
end
