class Photo::SimpleSerializer < ActiveModel::Serializer
	root :photo

	attributes :id, :description, :url, :comments_count

	has_one :location, serializer: Photo::LocationSerializer
	has_one :user, serializer: Photo::UserSerializer

	def url
		object.source.url :thumb
	end
end
