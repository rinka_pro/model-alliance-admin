class Photo::UserSerializer < ActiveModel::Serializer
	root :user
	attributes :id, :login
end
