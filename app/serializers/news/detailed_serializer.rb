class News::DetailedSerializer < ActiveModel::Serializer
	root :news

	attributes :id, :important, :additional_likes, :likes_count, :created_at

	has_many :photos, serializer: Photo::SharedSerializer
	has_many :cities, serializer: City::NameSerializer
	has_one :title
	has_one :description

	def photos
		object.shared_photos
	end
end
