class News::SimpleSerializer < ActiveModel::Serializer
	root :news

	attributes :id, :title, :created_at, :likes_count
	has_many :cities, serializer: City::NameSerializer

	def title
		object.title.ru || object.title.en
	end
end
