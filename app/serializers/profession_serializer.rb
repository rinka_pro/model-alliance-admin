class ProfessionSerializer < ActiveModel::Serializer
	root :profession
	attributes :id, :title

	def title
		object.ru
	end
end
