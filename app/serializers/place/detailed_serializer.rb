class Place::DetailedSerializer < ActiveModel::Serializer
	root :place

	attributes :id, :title, :description, :phone, :url, :logo, :worktime, :url

	has_one :location, serializer: Place::LocationSerializer
	has_many :shared_photos, serializer: Photo::SharedSerializer
	has_many :photos, serializer: Photo::SharedSerializer

	def logo
		object.logo.url :thumb
	end
end
