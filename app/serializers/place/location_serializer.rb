class Place::LocationSerializer < ActiveModel::Serializer
	root :location

	attributes :id, :latitude, :longitude, :category, :address
	has_one :city, serializer: City::LocalizedSerializer
end
