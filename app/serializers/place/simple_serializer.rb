class Place::SimpleSerializer < ActiveModel::Serializer
	root :place

	attributes :id, :phone, :title, :url
	has_one :location, serializer: Place::LocationSerializer
end
