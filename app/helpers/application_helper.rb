module ApplicationHelper
	def current_user
		@current_user ||= Core::User.where( id: session[ :user_id ] ).first
	end
end
