class ApplicationController < ActionController::Base
	include ApplicationHelper

	protect_from_forgery with: :exception
	layout false

	before_filter :check_authentication
	before_filter :include_layout

	private

	def include_layout
		render layout: 'application', text: '' unless angular? or xhr?
	end

	def angular?
		request.headers[ 'X-Angular' ] == 'true' and not xhr?
	end

	def xhr?
		request.headers[ 'Content-Type' ] == 'application/json'
	end

	def check_authentication
		redirect_to admins_path unless current_user
	end
end
