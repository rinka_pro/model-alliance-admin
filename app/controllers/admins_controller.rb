class AdminsController < ApplicationController
	skip_before_filter :check_authentication, only: :index
	skip_before_filter :include_layout, only: :index

	before_filter :skip_login, only: :index

	def index
		include_layout
	end

	private

	def skip_login
		redirect_to users_path if current_user
	end
end
