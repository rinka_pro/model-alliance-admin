class Angular::ModelAgenciesController < ApplicationController
	def index
		render json: Core::ModelAgency.includes( city: :title ), each_serializer: ModelAgencySerializer
	end
end
