class Angular::PlacesController < ApplicationController
	def index
		render json: Core::Place.includes( :title, location: { city: :title } ), each_serializer: Place::SimpleSerializer
	end

	def show
		render json: Core::Place.find( params[ :id ] ), serializer: Place::DetailedSerializer
	end
end
