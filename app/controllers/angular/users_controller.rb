class Angular::UsersController < ApplicationController
	def index
		users = Core::User.order created_at: :desc
		if page = params[ :page ]
			render json: users.paginate( page: page, per_page: 20 ), each_serializer: User::ListSerializer
		else
			users = ActiveModel::ArraySerializer.new users.limit( 20 ), serializer: User::ListSerializer
			render json: { users: users, count: Core::User.count }
		end
	end

	def show
		render json: Core::User.find( params[ :id ] ), serializer: User::DetailedSerializer
	end

	def professions
		render json: Core::LocalizationText.profession, each_serializer: ProfessionSerializer, root: :professions
	end
end
