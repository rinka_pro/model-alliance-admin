class Angular::Users::RoleUpdates::RequestsController < ApplicationController
	def index
		render json: Core::User::Role::UpdateRequest.all, each_serializer: User::RoleUpdate::ListSerializer, root: :role_update_requests
	end

	def show
		render json: Core::User::Role::UpdateRequest.find( params[ :id ] ), serializer: User::RoleUpdate::DetailedSerializer
	end
end
