class Angular::Users::RoleUpdates::StatesController < ApplicationController
	def update
		request = Core::User::Role::UpdateRequest.find params[ :id ]
		request.assign_attributes permitted_params
		if request.valid?
			request.approve
			render json: { success: true }
		else
			render json: request.errors.messages, status: 400
		end
	end

	private

	def permitted_params
		params.require( :role_update_state ).permit :birthday, :first_name, :last_name, model_agency_ids: []
	end
end
