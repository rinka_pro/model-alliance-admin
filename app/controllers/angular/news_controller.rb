class Angular::NewsController < ApplicationController
	def index
		render json: Core::News.includes( :title, cities: :title ), each_serializer: News::SimpleSerializer
	end

	def show
		render json: Core::News.find( params[ :id ] ), serializer: News::DetailedSerializer
	end
end
