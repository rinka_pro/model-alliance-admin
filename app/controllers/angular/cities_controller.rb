class Angular::CitiesController < ApplicationController
	def index
		render json: Core::City.includes( :title ), each_serializer: City::NameSerializer
	end
end
