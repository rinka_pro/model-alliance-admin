class Angular::PhotosController < ApplicationController
	def index
		render json: Core::Photo.includes( :location ), each_serializer: Photo::SimpleSerializer
	end
end
