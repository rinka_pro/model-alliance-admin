class Angular::AdminsController < ApplicationController
	skip_before_filter :check_authentication, only: :login

	def login
		user = Core::User.authenticate params
		if user.try :admin?
			session[ :user_id ] = user.id
			render json: user, serializer: Admin::LoginActionSerializer
		else
			render json: { error: 'Login or password invalid' }, status: 403
		end
	end
end
