class Angular::EventsController < ApplicationController
	def index
		render json: Core::Event.includes( :title, location: { city: :title } ), each_serializer: Event::SimpleSerializer
	end

	def show
		render json: Core::Event.find( params[ :id ] ), serializer: Event::DetailedSerializer
	end
end
