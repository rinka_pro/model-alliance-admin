class Angular::CommentsController < ApplicationController
	def index
		comments = Core::Comment.where( target_id: params[ :target_id ], target_type: params[ :target_type ] )
		render json: comments, each_serializer: CommentSerializer
	end
end
