require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require *Rails.groups

module Admin
	class Application < Rails::Application
		config.assets.js_compressor = :uglifier
		config.assets.paths << Rails.root.join( 'vendor', 'assets', 'bower_components' )

		config.i18n.load_path += Dir[ Core::Engine.root.join( 'config', 'locales', '**', '*.{rb,yml}' ).to_s ]
		config.i18n.load_path += Dir[ Rails.root.join( 'config', 'locales', '**', '*.{rb,yml}' ).to_s ]
		config.i18n.default_locale = :en

		config.active_record.raise_in_transactional_callbacks = true

		paths[ 'config/database' ] << Core::Engine.root + 'config/database.yml'

		config.generators do |g|
			g.test_framework :rspec,
				fixtures: true,
				view_specs: true,
				helper_specs: false,
				routing_specs: true,
				controller_specs: true,
				request_specs: true
			g.fixture_replacement :factory_girl, dir: 'spec/factories'
		end
	end
end
