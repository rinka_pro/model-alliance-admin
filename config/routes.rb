Rails.application.routes.draw do
	root 'admins#index'

	namespace :angular do
		namespace :users do
			namespace :role_updates do
				resources :requests, only: [ :index, :show ]
				resources :states, only: :update
			end
		end

		resources :users, only: [ :index, :show ] do
			collection do
				get :professions
			end
		end

		resources :admins, only: [] do
			collection do
				post :login
			end
		end

		resources :news, only: [ :index, :show ]
		resources :places, only: [ :index, :show ]
		resources :events, only: [ :index, :show ]
		resources :photos, only: [ :index, :show ]

		resources :model_agencies, only: :index
		resources :cities, only: :index

		resources :comments, only: [] do
			collection do
				get :index, path: '/:target_type/:target_id'
			end
		end
	end

	resources :role_update_requests, only: :index
	resources :admins, only: :index
	resources :users, only: [ :index, :show ]
	resources :places, only: [ :index, :show ]
	resources :events, only: [ :index, :show ]
	resources :photos, only: [ :index, :show ]
	resources :model_agencies, only: :index

	resources :news, only: [ :index, :show ] do
		collection do
			get :create, action: :show, path: '/create'
		end
	end
end
