# Custom
production = ENV[ 'RACK_ENV' ] == 'production'
app_path = production ? '/srv/model_alliance/admin/current' : '.'
pid_file = production ? '/run/alliance/admin.pid' : app_path + '/tmp/pids/unicorn.pid'

# Unicorn
worker_processes production ? 4 : 1
working_directory app_path

pid pid_file

stderr_path '/var/log/unicorn/alliance/admin/error.log' if production
stdout_path '/var/log/unicorn/alliance/admin/success.log' if production

timeout production ? 30 : 1800

listen '/srv/model_alliance/usocks/admin.sock', backlog: 1024

preload_app true

GC.copy_on_write_friendly = true if GC.respond_to? :copy_on_write_friendly=

before_exec do |server|
	ENV[ 'BUNDLE_GEMFILE' ] = "#{ rails_root }/Gemfile"
end

before_fork do |server, worker|
	Signal.trap 'TERM' do
		puts 'Unicorn master intercepting TERM and sending myself QUIT instead'
		Process.kill 'QUIT', Process.pid
	end

	# Before first worker starts, disconnect from DB
	ActiveRecord::Base.connection.disconnect! if defined? ActiveRecord::Base
end

after_fork do |server, worker|
	Signal.trap 'TERM' do
		puts 'Unicorn worker intercepting TERM and doing nothing. Wait for master to send QUIT'
	end

	# Connect DB after worker started
	ActiveRecord::Base.establish_connection if defined? ActiveRecord::Base
end
