# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151204125356) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "core_admin_messages", force: :cascade do |t|
    t.integer  "sender_id",                    null: false
    t.integer  "recipient_id"
    t.string   "text",                         null: false
    t.boolean  "read",         default: false, null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "core_admin_messages", ["created_at"], name: "index_core_admin_messages_on_created_at", order: {"created_at"=>:desc}, using: :btree
  add_index "core_admin_messages", ["recipient_id"], name: "index_core_admin_messages_on_recipient_id", using: :btree
  add_index "core_admin_messages", ["sender_id"], name: "index_core_admin_messages_on_sender_id", using: :btree

  create_table "core_admin_roles", force: :cascade do |t|
    t.integer "title", null: false
  end

  add_index "core_admin_roles", ["title"], name: "index_core_admin_roles_on_title", using: :btree

  create_table "core_castings", force: :cascade do |t|
    t.integer  "user_id",                                       null: false
    t.integer  "chest"
    t.string   "hair_color"
    t.integer  "height",     limit: 2
    t.integer  "hips",       limit: 2
    t.decimal  "shoe_size",             precision: 5, scale: 2
    t.integer  "zodiac",     limit: 2
    t.integer  "waist",      limit: 2
    t.integer  "weight",     limit: 2
    t.string   "eye_color",  limit: 30
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  add_index "core_castings", ["user_id"], name: "index_core_castings_on_user_id", using: :btree

  create_table "core_chat_members", force: :cascade do |t|
    t.integer  "chat_id",                      null: false
    t.integer  "member_id",                    null: false
    t.datetime "visited_at", default: "now()", null: false
  end

  add_index "core_chat_members", ["chat_id"], name: "index_core_chat_members_on_chat_id", using: :btree
  add_index "core_chat_members", ["member_id"], name: "index_core_chat_members_on_member_id", using: :btree

  create_table "core_chat_messages", force: :cascade do |t|
    t.integer  "sender_id",          null: false
    t.integer  "chat_id",            null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.text     "body"
    t.integer  "likes_count"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "core_chat_messages", ["chat_id"], name: "index_core_chat_messages_on_chat_id", using: :btree
  add_index "core_chat_messages", ["sender_id"], name: "index_core_chat_messages_on_sender_id", using: :btree

  create_table "core_chats", force: :cascade do |t|
    t.integer  "owner_id",                                null: false
    t.integer  "status",                  default: 0,     null: false
    t.integer  "variant",                 default: 0,     null: false
    t.boolean  "inappropriate",           default: false, null: false
    t.string   "title"
    t.integer  "role_priority", limit: 2,                 null: false
    t.integer  "gender",        limit: 2,                 null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "core_chats", ["gender"], name: "index_core_chats_on_gender", using: :btree
  add_index "core_chats", ["owner_id"], name: "index_core_chats_on_owner_id", using: :btree
  add_index "core_chats", ["role_priority"], name: "index_core_chats_on_role_priority", using: :btree

  create_table "core_checkins", force: :cascade do |t|
    t.integer  "user_id",                                 null: false
    t.integer  "discount_id",                             null: false
    t.integer  "selfie_id"
    t.string   "tags",        limit: 150
    t.boolean  "confirmed",               default: false, null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "core_checkins", ["discount_id"], name: "index_core_checkins_on_discount_id", using: :btree
  add_index "core_checkins", ["user_id"], name: "index_core_checkins_on_user_id", using: :btree

  create_table "core_cities", force: :cascade do |t|
    t.integer "country_id", null: false
  end

  add_index "core_cities", ["country_id"], name: "index_core_cities_on_country_id", using: :btree

  create_table "core_city_news", force: :cascade do |t|
    t.integer "city_id", null: false
    t.integer "news_id", null: false
  end

  add_index "core_city_news", ["city_id"], name: "index_core_city_news_on_city_id", using: :btree
  add_index "core_city_news", ["news_id"], name: "index_core_city_news_on_news_id", using: :btree

  create_table "core_comments", force: :cascade do |t|
    t.integer  "sender_id",                               null: false
    t.integer  "target_id",                               null: false
    t.string   "target_type",                             null: false
    t.integer  "role_priority", limit: 2,                 null: false
    t.integer  "gender",        limit: 2,                 null: false
    t.text     "body",                                    null: false
    t.boolean  "inappropriate",           default: false, null: false
    t.boolean  "moderated",               default: false, null: false
    t.integer  "rating"
    t.boolean  "read",                    default: false, null: false
    t.integer  "likes_count",             default: 0,     null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "core_comments", ["created_at"], name: "index_core_comments_on_created_at", order: {"created_at"=>:desc}, using: :btree
  add_index "core_comments", ["gender"], name: "index_core_comments_on_gender", using: :btree
  add_index "core_comments", ["role_priority"], name: "index_core_comments_on_role_priority", using: :btree
  add_index "core_comments", ["sender_id"], name: "index_core_comments_on_sender_id", using: :btree
  add_index "core_comments", ["target_type", "target_id"], name: "index_core_comments_on_target_type_and_target_id", using: :btree

  create_table "core_countries", force: :cascade do |t|
    t.string   "flag_file_name"
    t.string   "flag_content_type"
    t.integer  "flag_file_size"
    t.datetime "flag_updated_at"
  end

  create_table "core_descriptions", force: :cascade do |t|
    t.integer "target_id",                            null: false
    t.string  "target_type",                          null: false
    t.integer "variant",        limit: 2,             null: false
    t.integer "likes_count",              default: 0, null: false
    t.integer "comments_count",           default: 0, null: false
  end

  add_index "core_descriptions", ["target_type", "target_id"], name: "index_core_descriptions_on_target_type_and_target_id", using: :btree

  create_table "core_devices", primary_key: "device_id", force: :cascade do |t|
    t.integer "user_id",                                  null: false
    t.string  "device_token"
    t.string  "app_version",    limit: 20,                null: false
    t.integer "revision"
    t.string  "platform",       limit: 20,                null: false
    t.string  "os",             limit: 20,                null: false
    t.string  "mobile_version", limit: 20,                null: false
    t.boolean "push_enabled",              default: true, null: false
  end

  add_index "core_devices", ["user_id"], name: "index_core_devices_on_user_id", using: :btree

  create_table "core_discounts", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "place_id",                             null: false
    t.integer  "variant",        limit: 2,             null: false
    t.datetime "start"
    t.datetime "finish"
    t.integer  "comments_count",           default: 0, null: false
    t.integer  "gender",         limit: 2,             null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "core_discounts", ["gender"], name: "index_core_discounts_on_gender", using: :btree
  add_index "core_discounts", ["place_id"], name: "index_core_discounts_on_place_id", using: :btree
  add_index "core_discounts", ["role_id"], name: "index_core_discounts_on_role_id", using: :btree

  create_table "core_embassies", force: :cascade do |t|
    t.integer "country_origin_id", null: false
    t.string  "email",             null: false
    t.string  "phone",             null: false
  end

  add_index "core_embassies", ["country_origin_id"], name: "index_core_embassies_on_country_origin_id", using: :btree

  create_table "core_events", force: :cascade do |t|
    t.string   "phone"
    t.boolean  "important",                  default: false, null: false
    t.boolean  "show_on_map",                default: false, null: false
    t.datetime "start",                                      null: false
    t.datetime "finish",                                     null: false
    t.datetime "start_show"
    t.integer  "variant",          limit: 2,                 null: false
    t.string   "url"
    t.integer  "additional_likes", limit: 2, default: 0,     null: false
    t.integer  "likes_count",                default: 0,     null: false
    t.integer  "goings_count",               default: 0,     null: false
    t.integer  "comments_count",             default: 0,     null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "core_ibank_claims", force: :cascade do |t|
    t.integer  "user_id",                          null: false
    t.integer  "claim_id"
    t.integer  "status",     limit: 2, default: 0, null: false
    t.string   "message"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "core_ibank_claims", ["user_id"], name: "index_core_ibank_claims_on_user_id", using: :btree

  create_table "core_likes", force: :cascade do |t|
    t.integer  "user_id",                           null: false
    t.integer  "target_id",                         null: false
    t.string   "target_type",                       null: false
    t.integer  "variant",     limit: 2, default: 0, null: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "core_likes", ["user_id"], name: "index_core_likes_on_user_id", using: :btree

  create_table "core_links", force: :cascade do |t|
    t.integer "target_id",             null: false
    t.string  "target_type",           null: false
    t.integer "variant",     limit: 2, null: false
    t.string  "source",                null: false
  end

  add_index "core_links", ["target_type", "target_id"], name: "index_core_links_on_target_type_and_target_id", using: :btree

  create_table "core_localization_texts", force: :cascade do |t|
    t.integer "target_id",             null: false
    t.string  "target_type",           null: false
    t.integer "variant",     limit: 2
    t.string  "ru"
    t.string  "en"
    t.string  "de"
    t.string  "cn"
  end

  add_index "core_localization_texts", ["target_type", "target_id"], name: "index_core_localization_texts_on_target_type_and_target_id", using: :btree

  create_table "core_locations", force: :cascade do |t|
    t.integer "user_id"
    t.integer "target_id",                                                         null: false
    t.string  "target_type",                                                       null: false
    t.integer "city_id",                                                           null: false
    t.integer "role_priority", limit: 2
    t.integer "gender",        limit: 2
    t.decimal "latitude",                precision: 18, scale: 16,                 null: false
    t.decimal "longitude",               precision: 19, scale: 16,                 null: false
    t.integer "category",      limit: 2,                                           null: false
    t.boolean "show_on_map",                                       default: false, null: false
    t.string  "address"
  end

  add_index "core_locations", ["gender"], name: "index_core_locations_on_gender", using: :btree
  add_index "core_locations", ["latitude"], name: "index_core_locations_on_latitude", using: :btree
  add_index "core_locations", ["longitude"], name: "index_core_locations_on_longitude", using: :btree
  add_index "core_locations", ["role_priority"], name: "index_core_locations_on_role_priority", using: :btree
  add_index "core_locations", ["show_on_map"], name: "index_core_locations_on_show_on_map", using: :btree
  add_index "core_locations", ["user_id"], name: "index_core_locations_on_user_id", using: :btree

  create_table "core_model_agencies", force: :cascade do |t|
    t.integer "city_id", null: false
    t.string  "title",   null: false
  end

  add_index "core_model_agencies", ["city_id"], name: "index_core_model_agencies_on_city_id", using: :btree

  create_table "core_model_agency_targets", force: :cascade do |t|
    t.integer "agency_id",   null: false
    t.integer "target_id",   null: false
    t.string  "target_type", null: false
  end

  add_index "core_model_agency_targets", ["agency_id"], name: "index_core_model_agency_targets_on_agency_id", using: :btree
  add_index "core_model_agency_targets", ["target_type", "target_id"], name: "index_core_model_agency_targets_on_target_type_and_target_id", using: :btree

  create_table "core_model_roles", force: :cascade do |t|
    t.integer "title",              null: false
    t.integer "priority", limit: 2, null: false
  end

  add_index "core_model_roles", ["title"], name: "index_core_model_roles_on_title", using: :btree

  create_table "core_news", force: :cascade do |t|
    t.boolean  "important",                  default: false, null: false
    t.integer  "additional_likes",           default: 0,     null: false
    t.integer  "likes_count",                default: 0,     null: false
    t.integer  "comments_count",             default: 0,     null: false
    t.integer  "role_priority",    limit: 2
    t.integer  "gender",           limit: 2,                 null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "core_news", ["gender"], name: "index_core_news_on_gender", using: :btree
  add_index "core_news", ["role_priority"], name: "index_core_news_on_role_priority", using: :btree

  create_table "core_photos", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "target_id"
    t.string   "target_type"
    t.integer  "variant",                       default: 0, null: false
    t.string   "source_file_name",                          null: false
    t.string   "source_content_type",                       null: false
    t.integer  "source_file_size",                          null: false
    t.datetime "source_updated_at",                         null: false
    t.string   "description"
    t.integer  "likes_count",                   default: 0, null: false
    t.integer  "comments_count",                default: 0, null: false
    t.integer  "role_priority",       limit: 2
    t.integer  "gender",              limit: 2
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "core_photos", ["gender"], name: "index_core_photos_on_gender", using: :btree
  add_index "core_photos", ["role_priority"], name: "index_core_photos_on_role_priority", using: :btree
  add_index "core_photos", ["target_type", "target_id"], name: "index_core_photos_on_target_type_and_target_id", using: :btree
  add_index "core_photos", ["user_id"], name: "index_core_photos_on_user_id", using: :btree
  add_index "core_photos", ["variant"], name: "index_core_photos_on_variant", using: :btree

  create_table "core_places", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "admin_id"
    t.integer  "partner_id"
    t.boolean  "inappropriate",                 default: false, null: false
    t.string   "phone",             limit: 30
    t.integer  "rating",            limit: 2,   default: 0
    t.string   "url"
    t.string   "worktime"
    t.string   "tags",              limit: 200
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.integer  "likes_count",                   default: 0,     null: false
    t.integer  "comments_count",                default: 0,     null: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  add_index "core_places", ["admin_id"], name: "index_core_places_on_admin_id", using: :btree

  create_table "core_shared_photos", force: :cascade do |t|
    t.integer  "target_id",           null: false
    t.string   "target_type",         null: false
    t.string   "source_file_name",    null: false
    t.string   "source_content_type", null: false
    t.integer  "source_file_size",    null: false
    t.datetime "source_updated_at",   null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "core_shared_photos", ["target_type", "target_id"], name: "index_core_shared_photos_on_target_type_and_target_id", using: :btree

  create_table "core_user_logins", force: :cascade do |t|
    t.integer  "user_id",                                  null: false
    t.string   "device_id",                                null: false
    t.string   "app_version",    limit: 20,                null: false
    t.integer  "revision",                                 null: false
    t.string   "platform",       limit: 20,                null: false
    t.string   "os",             limit: 20,                null: false
    t.string   "mobile_version", limit: 20,                null: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "locale",         limit: 3,  default: "en", null: false
  end

  add_index "core_user_logins", ["device_id"], name: "index_core_user_logins_on_device_id", using: :btree
  add_index "core_user_logins", ["user_id"], name: "index_core_user_logins_on_user_id", using: :btree

  create_table "core_user_role_admin_links", force: :cascade do |t|
    t.integer "admin_id", null: false
    t.integer "role_id",  null: false
  end

  add_index "core_user_role_admin_links", ["admin_id"], name: "index_core_user_role_admin_links_on_admin_id", using: :btree
  add_index "core_user_role_admin_links", ["role_id"], name: "index_core_user_role_admin_links_on_role_id", using: :btree

  create_table "core_user_role_update_requests", force: :cascade do |t|
    t.integer  "user_id",                                   null: false
    t.integer  "role_id",                                   null: false
    t.date     "birthday"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.string   "selfie_file_name"
    t.string   "selfie_content_type"
    t.integer  "selfie_file_size"
    t.datetime "selfie_updated_at"
    t.integer  "gender",              limit: 2, default: 1, null: false
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "core_user_role_update_requests", ["role_id"], name: "index_core_user_role_update_requests_on_role_id", using: :btree
  add_index "core_user_role_update_requests", ["user_id"], name: "index_core_user_role_update_requests_on_user_id", using: :btree

  create_table "core_user_social_logins", force: :cascade do |t|
    t.integer  "user_id",                  null: false
    t.string   "user_social_id",           null: false
    t.string   "token",                    null: false
    t.integer  "social_title",   limit: 2, null: false
    t.datetime "expiration"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "core_user_social_logins", ["token"], name: "index_core_user_social_logins_on_token", using: :btree
  add_index "core_user_social_logins", ["user_id"], name: "index_core_user_social_logins_on_user_id", using: :btree
  add_index "core_user_social_logins", ["user_social_id"], name: "index_core_user_social_logins_on_user_social_id", using: :btree

  create_table "core_users", force: :cascade do |t|
    t.integer  "role_id",                                               null: false
    t.integer  "city_id"
    t.string   "login"
    t.string   "password_hash",               limit: 60
    t.integer  "gender",                      limit: 2,  default: 1,    null: false
    t.string   "passport_photo_file_name"
    t.string   "passport_photo_content_type"
    t.integer  "passport_photo_file_size"
    t.datetime "passport_photo_updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "selfie_file_name"
    t.string   "selfie_content_type"
    t.integer  "selfie_file_size"
    t.datetime "selfie_updated_at"
    t.date     "birthday"
    t.string   "email",                                                 null: false
    t.string   "first_name"
    t.boolean  "helpful",                                default: true, null: false
    t.string   "last_name"
    t.string   "patronymic"
    t.string   "phone",                       limit: 30
    t.boolean  "receive_invitations",                    default: true, null: false
    t.boolean  "show_login",                             default: true, null: false
    t.string   "skype",                       limit: 40
    t.string   "social_status"
    t.string   "mood_status"
    t.string   "website"
    t.boolean  "banned"
    t.datetime "expiration_ban"
    t.string   "birth_city"
    t.integer  "likes_count",                            default: 0,    null: false
    t.integer  "comments_count",                         default: 0,    null: false
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "profession",                  limit: 30
  end

  add_index "core_users", ["email"], name: "index_core_users_on_email", using: :btree
  add_index "core_users", ["login"], name: "index_core_users_on_login", using: :btree
  add_index "core_users", ["role_id"], name: "index_core_users_on_role_id", using: :btree

  create_table "mobile_sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "mobile_sessions", ["session_id"], name: "index_mobile_sessions_on_session_id", using: :btree
  add_index "mobile_sessions", ["updated_at"], name: "index_mobile_sessions_on_updated_at", using: :btree

  add_foreign_key "core_admin_messages", "core_users", column: "recipient_id", on_delete: :cascade
  add_foreign_key "core_admin_messages", "core_users", column: "sender_id", on_delete: :cascade
  add_foreign_key "core_castings", "core_users", column: "user_id", on_delete: :cascade
  add_foreign_key "core_chat_members", "core_chats", column: "chat_id", on_delete: :cascade
  add_foreign_key "core_chat_members", "core_users", column: "member_id", on_delete: :cascade
  add_foreign_key "core_chat_messages", "core_users", column: "sender_id", on_delete: :cascade
  add_foreign_key "core_chats", "core_users", column: "owner_id"
  add_foreign_key "core_checkins", "core_discounts", column: "discount_id", on_delete: :cascade
  add_foreign_key "core_checkins", "core_users", column: "user_id", on_delete: :cascade
  add_foreign_key "core_cities", "core_countries", column: "country_id"
  add_foreign_key "core_city_news", "core_cities", column: "city_id", on_delete: :cascade
  add_foreign_key "core_city_news", "core_news", column: "news_id", on_delete: :cascade
  add_foreign_key "core_comments", "core_users", column: "sender_id", on_delete: :cascade
  add_foreign_key "core_devices", "core_users", column: "user_id", on_delete: :cascade
  add_foreign_key "core_discounts", "core_model_roles", column: "role_id"
  add_foreign_key "core_discounts", "core_places", column: "place_id", on_delete: :cascade
  add_foreign_key "core_embassies", "core_countries", column: "country_origin_id"
  add_foreign_key "core_ibank_claims", "core_users", column: "user_id", on_delete: :cascade
  add_foreign_key "core_likes", "core_users", column: "user_id", on_delete: :cascade
  add_foreign_key "core_locations", "core_cities", column: "city_id"
  add_foreign_key "core_locations", "core_users", column: "user_id", on_delete: :cascade
  add_foreign_key "core_model_agencies", "core_cities", column: "city_id"
  add_foreign_key "core_model_agency_targets", "core_model_agencies", column: "agency_id", on_delete: :cascade
  add_foreign_key "core_photos", "core_users", column: "user_id", on_delete: :cascade
  add_foreign_key "core_places", "core_users", column: "admin_id", on_delete: :cascade
  add_foreign_key "core_places", "core_users", column: "partner_id", on_delete: :cascade
  add_foreign_key "core_places", "core_users", column: "user_id", on_delete: :cascade
  add_foreign_key "core_user_logins", "core_devices", column: "device_id", primary_key: "device_id"
  add_foreign_key "core_user_logins", "core_users", column: "user_id", on_delete: :cascade
  add_foreign_key "core_user_role_admin_links", "core_admin_roles", column: "role_id", on_delete: :cascade
  add_foreign_key "core_user_role_admin_links", "core_users", column: "admin_id", on_delete: :cascade
  add_foreign_key "core_user_role_update_requests", "core_model_roles", column: "role_id", on_delete: :cascade
  add_foreign_key "core_user_role_update_requests", "core_users", column: "user_id", on_delete: :cascade
  add_foreign_key "core_user_social_logins", "core_users", column: "user_id", on_delete: :cascade
  add_foreign_key "core_users", "core_model_roles", column: "role_id"
end
